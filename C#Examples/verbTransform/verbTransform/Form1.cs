﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace verbTransform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Check the verb entered across all methods
            addD();
            addY();
            doubleLetter();
            allOthers();
        }


        private bool addD()
        {
            string verb = textBox1.Text;  //Get user input
            int count = 0;
            foreach (char a in verb)  //Check each character in the input string to ensure it is a valid letter
                if (char.IsLetter(a))
                {
                    count++;
                }
            if (count == verb.Length)  //if the string is all letters, check for conditions
            {
                int lastLetter = verb.Length;
                int hasE;
                int hasUe;
                hasE = verb.LastIndexOf("e");
                hasUe = verb.LastIndexOf("ue");
                if (hasE == lastLetter - 1 || hasUe == lastLetter - 2)
                {
                    verb = verb.Insert(verb.Length, "d");
                    label2.Text = verb;
                    return true;
                }
            }
            else label2.Text = "You did not enter a proper string value.";
            return false;
        }

        private bool addY()
        {
            string verb = textBox1.Text;  //Get user input
            int count = 0;
            foreach (char a in verb)   //Check each character in the input string to ensure it is a valid letter
                if (char.IsLetter(a))
                {
                    count++;
                }

            if (count == verb.Length)  //if the string is all letters, check for conditions
            {
                int lastLetter = verb.Length;
                int hasY;
                hasY = verb.LastIndexOf("y");
                if (hasY == lastLetter - 1)
                {
                    verb = verb.Remove(hasY);
                    verb = verb.Insert(verb.Length, "ied");
                    label2.Text = verb;
                    return true;
                }
            }
            else label2.Text = "You did not enter a proper string value.";
            return false;
        }

        private bool doubleLetter()
        {
            string verb = textBox1.Text;  //Get user input 
            int count = 0;
            foreach (char a in verb)  //Check each character in the input string to ensure it is a valid letter
                if (char.IsLetter(a))
                {
                    count++;
                }
            if (count == verb.Length)  //if the string is all letters, check for conditions
            {
                int lastLetter = verb.Length;
                int hasVowelConst = lastLetter - 2;
                char hasVowel;
                string[] vowels = { "a", "e", "i", "o", "u" };

                foreach (string str in vowels)
                {
                    if (verb.IndexOf(str) == hasVowelConst)
                    {
                        hasVowel = verb[lastLetter - 1];
                        verb = verb.Insert(verb.Length, hasVowel + "ed");
                        label2.Text = verb;
                        return true;
                    }
                }
            }
            else label2.Text = "You did not enter a proper string value.";
            return false;
        }

        private void allOthers()  // if verb did not meet any previous values, at ed
        {
            if (!addD() && !addY() && !doubleLetter())
            {
                string verb = textBox1.Text;
                int count = 0;
                foreach (char a in verb)
                    if (char.IsLetter(a))
                    {
                        count++;
                    }
                if (count == verb.Length)
                {
                    verb = verb.Insert(verb.Length, "ed");
                    label2.Text = verb;
                }
                else label2.Text = "You did not enter a proper string value.";
            }
        }
    }
}
