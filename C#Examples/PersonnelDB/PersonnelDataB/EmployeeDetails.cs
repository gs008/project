﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonnelDataB
{
    public partial class EmployeeDetails : Form
    {
        public EmployeeDetails()
        {
            InitializeComponent();
        }

        private void employeeBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.employeeBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.personnelDBDataSet);

        }

        private void EmployeeDetails_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'personnelDBDataSet.Employee' table. You can move, or remove it, as needed.
            this.employeeTableAdapter.Fill(this.personnelDBDataSet.Employee);

        }

        private void sortByPayButton_Click(object sender, EventArgs e)
        {
            this.employeeTableAdapter.SortRate(this.personnelDBDataSet.Employee);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            //Update the dataset
            this.employeeTableAdapter.Fill(this.personnelDBDataSet.Employee);

            //Close the form
            this.Close();
        }

        private void descendingPayButton_Click(object sender, EventArgs e)
        {
            this.employeeTableAdapter.DescRate(this.personnelDBDataSet.Employee);

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.employeeTableAdapter.SearchName(this.personnelDBDataSet.Employee, textBox1.Text);
        }

        private void highestButton_Click(object sender, EventArgs e)
        {
            //Variable to hold Highest Rate
            double highestRate;

            //Get the Highest Rate
            highestRate = (double) this.employeeTableAdapter.Highest();

            //Show the message
            MessageBox.Show("The Highest Pay Rate is " + highestRate.ToString("c"));
        }

        private void lowestButton_Click(object sender, EventArgs e)
        {
            //Variable to hold the lowest rate
            double lowestRate;

            //Get the Lowest Rate
            lowestRate = (double) this.employeeTableAdapter.Lowest();
            //Show the message
            MessageBox.Show("The Lowest Pay Rate is " + lowestRate.ToString("c"));

        }
    }
}
