﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonnelDataB
{
    public partial class GridView : Form
    {
        public GridView()
        {
            InitializeComponent();
        }

        private void employeeBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.employeeBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.personnelDBDataSet);

        }

        private void GridView_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'personnelDBDataSet.Employee' table. You can move, or remove it, as needed.
            this.employeeTableAdapter.Fill(this.personnelDBDataSet.Employee);

        }

        private void detailsButton_Click(object sender, EventArgs e)
        {
            //New instance of EmployeeForm
            EmployeeDetails details = new EmployeeDetails();

            //Display the Form
            details.ShowDialog();

            //Update the dataset
            this.employeeTableAdapter.Fill(this.personnelDBDataSet.Employee);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            //Close the form
            this.Close();
        }
    }
}
