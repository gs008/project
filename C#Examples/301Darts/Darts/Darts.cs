﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Darts
{
    public class Dart
    {
        public int Score { get; set; }
        public bool IsDouble { get; set; }
        public bool IsTriple { get; set; }
        public bool halfBullsEye { get; set; }
        public bool fullBullsEye { get; set; }


        private Random _random;

        public Dart(Random random)
        {
            _random = random;
        }

        public void Throw()
        {
            Score = _random.Next(0, 21);
            int ScoreMultiplier = _random.Next(1, 21);

            if (Score == 0 && ScoreMultiplier == 1) fullBullsEye = true;
            else if (Score == 0 && ScoreMultiplier != 1) halfBullsEye = true;
            else if (Score != 0 && ScoreMultiplier == 2) IsDouble = true;
            else if (Score != 0 && ScoreMultiplier == 3) IsTriple = true;
           
        }

    }
}
