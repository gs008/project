﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DartsSecondAttempt
{
    public partial class Default : System.Web.UI.Page
    {
        int newScore;
        int otherScore;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                
            }
            if (ViewState["moneyUpdate"] != null)
            {
                newScore = (int)ViewState["moneyUpdate"];
                otherScore = (int)ViewState["scoreUpdate"];
            }
        }

        protected void okButton_Click(object sender, EventArgs e)
        {
            newGame();
        }
        public void playerChecker(Game game)
        {
            if (game.playChecker == true)
            {
                Label1.Text = newScore.ToString();
                p1t1.Text = game.values[0].ToString();
                p1t2.Text = game.values[1].ToString();
                p1t3.Text = game.values[2].ToString();
            }
            else
            {
                Label2.Text = otherScore.ToString();
                p2t1.Text = game.values[0].ToString();
                p2t2.Text = game.values[1].ToString();
                p2t3.Text = game.values[2].ToString();
            }
        }
        public void newGame()
        {
            Game dartsGame = new Game("Greg", "Nikki");
            startGame(dartsGame, dartsGame._player1);
            playerChecker(dartsGame);
            startGame(dartsGame, dartsGame._player2);
            playerChecker(dartsGame);
            player1Label.Text = dartsGame._player1.Name;
            player2Label.Text = dartsGame._player2.Name;
            determineWinner(dartsGame);
        }
        public void startGame(Game game, Player player)
        {
            game.Play(player);
            if (player == game._player1)
            {
                newScore += game._player1.Score;
                ViewState.Add("moneyUpdate", newScore);
            }
            else
            {
                otherScore += game._player2.Score;
                ViewState.Add("scoreUpdate", otherScore);
            }
        }

        public void determineWinner(Game game)
        {
            if (newScore <= 300 && otherScore <= 300) resultLabel.Text = "Next Round";
            else
            {
                resultLabel.Text = game.displayResults(checkWinner(game));
                okButton.Enabled = false;
            }
        }
        public Player checkWinner(Game game)
        {
            if (newScore > otherScore) return game._player1;
            else return game._player2;
        }

    }
}