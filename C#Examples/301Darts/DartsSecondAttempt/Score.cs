﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Darts;

namespace DartsSecondAttempt
{
    public class Score
    {
        public static void ScoreDart(Player player, Dart dart)
        {
            int Score = 0;

            if (dart.IsDouble) Score = dart.Score * 2;
            else if (dart.IsTriple) Score = dart.Score * 3;
            else if (dart.halfBullsEye) Score = 25;
            else if (dart.fullBullsEye) Score = 50;
            else Score = dart.Score;
            player.Score += Score;
            player.Number = Score;
        }
    }
}