﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Darts;

namespace DartsSecondAttempt
{
    public class Game
    {
        public Player _player1;
        public Player _player2;
        public int[] values = new int[3];
        private Random _random;
        public bool playChecker;

        public Game(string player1Name, string player2Name)
        {
            _player1 = new Player();
            _player1.Name = player1Name;
            _player2 = new Player();
            _player2.Name = player2Name;
            _random = new Random();
        }
       
        public void Play(Player player)
        {
                playRound(player);
        }
        private void checkPlayer(Player player)
        {
            if (player == _player1) playChecker = true;
            else playChecker = false;
        }
        private void playRound(Player player)
        {
            for (int i = 0; i < 3; i++)
            {
                Dart dart = new Dart(_random);
                dart.Throw();
                Score.ScoreDart(player, dart);
                checkPlayer(player);
                values[i] = player.Number;
            }
        }

        public string displayResults(Player player)
        {
            string message = "<p style=\"color:red; font-size:2em;\">Winner!!</p>" + player.Name;
            return message; 
        }
    }
}