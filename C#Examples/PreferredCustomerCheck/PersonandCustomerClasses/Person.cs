﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonandCustomerClasses
{
    public class Person
    {
        //Propeties
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        //Constructor
        public Person()
        {

        }

        //Overloaded Constructor
        public Person(string name, string address, string phone)
        {
            Name = name;
            Address = address;
            Phone = phone;
        }
    }
}
