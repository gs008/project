﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonandCustomerClasses
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            string name = "";  //Variables to store user input
            string address = "";
            string phone = "";
            int num;
            bool mail = false;

            if (int.TryParse(customerNumberTextBox.Text, out num))  //Check for valid inputs
            {
                name = nameTextBox.Text;
                address = addressTextBox.Text;
                phone = PhoneNumberTextBox.Text;
                if (mailCheckBox.Checked)
                {
                    mail = true;
                } else mail = false;
            }
            else MessageBox.Show("Please enter a valid Customer Number.");  //Error message
            
            Customer cust = new Customer(num, mail);  //create customer object and assign values
            cust.Name = name;
            cust.Address = address;
            cust.Phone = phone;
            cust.MailPreference = mail;

            Preferred checkedCustomer = new Preferred();  //create a preferred form object and display output
            checkedCustomer.nameLabel.Text = cust.Name;
            checkedCustomer.addressLabel.Text = cust.Address;
            checkedCustomer.phoneLabel.Text = cust.Phone;
            checkedCustomer.numberLabel.Text = cust.CustomerNumber.ToString();
            bool wantsMail = cust.MailPreference;
            if (wantsMail)
            {
                checkedCustomer.mailLabel.Text = "Please Mail";
            }
            else checkedCustomer.mailLabel.Text = "Do Not Mail";
            checkedCustomer.newCustomer(cust);
            if (num > 0)
            {
                checkedCustomer.ShowDialog();
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            //Close the form
            this.Close();
        }
    }
}
