﻿namespace PersonandCustomerClasses
{
    partial class Receipt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.totalDiscountLabel = new System.Windows.Forms.Label();
            this.discountLabel = new System.Windows.Forms.Label();
            this.nameLabel = new System.Windows.Forms.Label();
            this.closeButon = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.purchasesLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(154, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Receipt";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(123, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(106, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Discount :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(82, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Total Discount:";
            // 
            // totalDiscountLabel
            // 
            this.totalDiscountLabel.AutoSize = true;
            this.totalDiscountLabel.Location = new System.Drawing.Point(212, 229);
            this.totalDiscountLabel.Name = "totalDiscountLabel";
            this.totalDiscountLabel.Size = new System.Drawing.Size(35, 13);
            this.totalDiscountLabel.TabIndex = 4;
            this.totalDiscountLabel.Text = "label5";
            // 
            // discountLabel
            // 
            this.discountLabel.AutoSize = true;
            this.discountLabel.Location = new System.Drawing.Point(212, 177);
            this.discountLabel.Name = "discountLabel";
            this.discountLabel.Size = new System.Drawing.Size(35, 13);
            this.discountLabel.TabIndex = 5;
            this.discountLabel.Text = "label6";
            // 
            // nameLabel
            // 
            this.nameLabel.AutoSize = true;
            this.nameLabel.Location = new System.Drawing.Point(212, 87);
            this.nameLabel.Name = "nameLabel";
            this.nameLabel.Size = new System.Drawing.Size(35, 13);
            this.nameLabel.TabIndex = 6;
            this.nameLabel.Text = "label7";
            // 
            // closeButon
            // 
            this.closeButon.Location = new System.Drawing.Point(153, 272);
            this.closeButon.Name = "closeButon";
            this.closeButon.Size = new System.Drawing.Size(75, 23);
            this.closeButon.TabIndex = 7;
            this.closeButon.Text = "Close";
            this.closeButon.UseVisualStyleBackColor = true;
            this.closeButon.Click += new System.EventHandler(this.closeButon_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(101, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Purchases:";
            // 
            // purchasesLabel
            // 
            this.purchasesLabel.AutoSize = true;
            this.purchasesLabel.Location = new System.Drawing.Point(212, 134);
            this.purchasesLabel.Name = "purchasesLabel";
            this.purchasesLabel.Size = new System.Drawing.Size(35, 13);
            this.purchasesLabel.TabIndex = 9;
            this.purchasesLabel.Text = "label7";
            // 
            // Receipt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 434);
            this.Controls.Add(this.purchasesLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.closeButon);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.discountLabel);
            this.Controls.Add(this.totalDiscountLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Receipt";
            this.Text = "Receipt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button closeButon;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label purchasesLabel;
        public System.Windows.Forms.Label totalDiscountLabel;
        public System.Windows.Forms.Label discountLabel;
        public System.Windows.Forms.Label nameLabel;
    }
}