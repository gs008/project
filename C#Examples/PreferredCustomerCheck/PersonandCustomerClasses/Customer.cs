﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonandCustomerClasses
{
    public class Customer : Person
    {
        //Properties
        public int CustomerNumber { get; set; }
        public bool MailPreference { get; set; }

        //Constructor
        public Customer(int num, bool mail)
        {
            CustomerNumber = num;
            MailPreference = mail;
        }

    }
}
