﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonandCustomerClasses
{
    public class PreferredCustomer : Customer
    {
        //Properties
        public double Purchases { get; set; }
        public int Discount { get; set; }


        public PreferredCustomer(int num, bool mail, double purchases, int discount) : base(num, mail)  //Constructor method
        {
            Purchases = purchases;
            Discount = discount;
        }

    }
}
