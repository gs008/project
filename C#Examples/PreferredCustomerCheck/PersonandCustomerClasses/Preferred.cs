﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonandCustomerClasses
{
    public partial class Preferred : Form  //Form gets created for a preferred customer
    {
        public Customer NewCustomer { get; set; }
        public Preferred()
        {
            InitializeComponent();
        }

       
        public Customer newCustomer(Customer cust)  //Pass in a customer
        {
            NewCustomer = cust;
            return NewCustomer;
        }

        private PreferredCustomer checkCustomer(Customer cust)  //Determine the custoemrs discount, if any
        {
            double purchase;
            int discount = 0;
            if (double.TryParse(purchaseTextBox.Text, out purchase))
            {
                if (purchase > 0)
                {

                    if ((purchase >= 500) && (purchase < 1000))
                    {
                        discount = 5;
                    }
                    else if ((purchase >= 1000) && (purchase < 1500))
                    {
                        discount = 6;
                    }
                    else if ((purchase >= 1500) && (purchase < 2000))
                    {
                        discount = 7;
                    }
                    else if (purchase >= 2000)
                    {
                        discount = 10;
                    }
                    else discount = 0;
                }
                else MessageBox.Show("Please enter a proper purchase amount.");  //Error message
                 
            }
            else MessageBox.Show("Please enter a valid Purchase amount");  //Error message

            PreferredCustomer customer = new PreferredCustomer(cust.CustomerNumber, cust.MailPreference, purchase, discount);  //create a new preferredCustomer object
            customer.Name = cust.Name;
            customer.Address = cust.Address;
            return customer;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {

            PreferredCustomer preferred = checkCustomer(NewCustomer); //Pass the preferredCustomer object to preferred variable
            if (preferred.Purchases > 0)
            {
                Receipt custReceipt = new Receipt();  //Create a custReceipt form object to display results
                double discount = (double)preferred.Discount;
                discount /= 100;
                custReceipt.nameLabel.Text = preferred.Name;
                custReceipt.purchasesLabel.Text = preferred.Purchases.ToString("c");
                custReceipt.discountLabel.Text = preferred.Discount.ToString() + "%";
                custReceipt.totalDiscountLabel.Text = (preferred.Purchases * discount).ToString("c");
                custReceipt.ShowDialog();
            }
            else MessageBox.Show("You did not enter the correct information to generate a receipt.");  //Error message
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            //Close the form
            this.Close();
        }
    }
}
