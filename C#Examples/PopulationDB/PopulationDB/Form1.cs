﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PopulationDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cityBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.cityBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.populationDBDataSet);

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'populationDBDataSet.City' table. You can move, or remove it, as needed.
            this.cityTableAdapter.Fill(this.populationDBDataSet.City);

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.cityTableAdapter.searchAny(this.populationDBDataSet.City, textBox1.Text);
        }

        private void popAscendingButton_Click(object sender, EventArgs e)
        {
            this.cityTableAdapter.AscendingPop(this.populationDBDataSet.City);
        }

        private void popDescendingButton_Click(object sender, EventArgs e)
        {
            this.cityTableAdapter.DescendingPop(this.populationDBDataSet.City);
        }


        private void totalPopButton_Click(object sender, EventArgs e)
        {
            double total = (double) this.cityTableAdapter.SumPopulation();
            MessageBox.Show("The Total Population of all cities is " + total.ToString("n") + " people.");
        }

        private void averagePopButton_Click(object sender, EventArgs e)
        {
            double avg = (double)this.cityTableAdapter.AveragePop();
            MessageBox.Show("The Average Population of all cities is " + avg.ToString("n") + " people.");
        }

        private void highestPopButton_Click(object sender, EventArgs e)
        {
            int highest = (int)this.cityTableAdapter.HighestPop();
            this.cityTableAdapter.thisCity(this.populationDBDataSet.City, highest);
        }

        private void lowestPopButton_Click(object sender, EventArgs e)
        {
            int lowest = (int)this.cityTableAdapter.LowestPop();
            this.cityTableAdapter.thisCity(this.populationDBDataSet.City, lowest);
        }

        private void alphabeticalCityButton_Click(object sender, EventArgs e)
        {
            this.cityTableAdapter.OrderByCity(this.populationDBDataSet.City);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
           
            this.cityTableAdapter.Fill(this.populationDBDataSet.City);
        }
    }
}
