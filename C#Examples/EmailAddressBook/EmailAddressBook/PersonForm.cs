﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EmailAddressBook
{
    public partial class PersonForm : Form
    {

        public PersonForm()  
        {
            InitializeComponent();
        }
        public void populateList(PersonEntry person)  //Update form labels with Property Values
        {
            nameLabel.Text += " " + person.PersonName;
            mailLabel.Text += " " + person.Email;
            addLabel.Text += " " + person.Address;
            phoneLabel.Text += " " + person.Phone;
        }

        private void button1_Click(object sender, EventArgs e) //Close this form
        {
            this.Close();
        }
    }
}
