﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailAddressBook
{
    public class PersonEntry
    {
        //Properties 
        public string PersonName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public PersonEntry(string name, string mail, string add, string phone)  //Constructor to initialize Property values
        {
            PersonName = name;
            Email = mail;
            Address = add;
            Phone = phone;
        }

    }
}
