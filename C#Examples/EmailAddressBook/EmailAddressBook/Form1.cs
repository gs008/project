﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EmailAddressBook
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            firstNames(Info());  //Call this method when the new form object gets created
        }

        private List<PersonEntry> Info()  //Create a list of PersonEntry objects that get created as the text file is read.
        {
            StreamReader readFile;  //Declare a StreamReader object
            string line;  //To hold text from the current line in the file
            char[] delim = { ',' }; //Character used to split up text line
           
            List<PersonEntry> People = new List<PersonEntry>();  //Create the List object

            readFile = File.OpenText("person.txt");  //Open the file

            while (!readFile.EndOfStream)  //As long as the file is not to the end, perform actions
            {
                line = readFile.ReadLine();              //Read the next line in the text file
                string[] tokens = line.Split(delim);     //At each ',' character, split the line into an array 
                PersonEntry person = new PersonEntry(tokens[0],tokens[1], tokens[2], tokens[3]);  //Create a new PersonEntry object
                People.Add(person);                      //Add the object to the earlier list created
            }
            readFile.Close();    //Close the file
            return People;       //Returns the entire list of PersonEntry objects
        }

        private void firstNames(List<PersonEntry> People)  //Pass in a List of PersonEntry objects as an argument
        {
            foreach (PersonEntry p in People)  //Loop through each object in the list and add their name property to the listbox
            {
                listBox1.Items.Add(p.PersonName);
            }
        }

        private void selectedPerson(object sender, EventArgs e)  //When a user clicks a name, perform the actions
        {
            int person = listBox1.SelectedIndex;  //Get the index of the selected name
            List<PersonEntry> allPeople = Info(); //Get a list of all possible PersonEntry objects
            PersonEntry currentPerson = allPeople[person];  //Select the PersonEntry object that has the same index as the user selected
            PersonForm p = new PersonForm(); //create a new PersonForm object and pass in the string array as an argument
            p.populateList(currentPerson);
            p.ShowDialog();  //Display the PersonForm
        }
    }
}
