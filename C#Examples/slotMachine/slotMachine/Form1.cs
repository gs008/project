﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace slotMachine
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //Generate the first three slot images on page load.
            changeImage(rand, pictureBox1);
            changeImage(rand, pictureBox2);
            changeImage(rand, pictureBox3);
        }

        //Random object to use when generating images
        Random rand = new Random();
        
        //Used to total amount user enters
        double inserted = 0;

        //Used to total amount won
        double won = 0;

        //Pass in a random number and a picturebox, assign the picturebox the 
        //image that is in the index value held by the random number. 
        //Also, return the index value
        private int changeImage(Random rand, PictureBox a)
        {
            int index = rand.Next(imageList1.Images.Count);
            a.Image = imageList1.Images[index];
            return index;
        }

        //Pass in the index value for each of the images used to populate picturebox's.  
        //Compare the index values and assign the appropriate winning if applicable.
        //return the amount won, if any.
        private double money(int image1Index, int image2Index, int image3Index)
        {
            double bet = 0.0;
            if (double.TryParse(textBox1.Text, out bet))
            {
                inserted += bet;
                if ((image1Index == image2Index) || (image1Index == image3Index) || (image2Index == image3Index))
                {
                    bet *= 2;
                    won += bet;
                }
                else if ((image1Index == image2Index) && (image1Index == image3Index) && (image2Index == image3Index))
                {
                    bet *= 3;
                    won += bet;
                }
                else bet = 0;

            }
            else MessageBox.Show("Please enter a proper bet!");
            return bet;

        }

        //Display to the user the winnings, if any.
        //Clear the textbox.
        private void button1_Click(object sender, EventArgs e)
        {
            label3.Text = String.Format("You won: {0} on that spin", money(changeImage(rand, pictureBox1), changeImage(rand, pictureBox2), changeImage(rand, pictureBox3)).ToString("C"));
            textBox1.Text = "";
        }
        
        //When user exits, show them how much they entered into the machine total, as well as the total amount won.
        //If the user won more than they lost, congradulate them.
        private void exitMessage()
        {
            MessageBox.Show("The total amount of money entered into the machine was $" + inserted + ".  The total amount won was $" + won + ".");
            if (won > inserted) MessageBox.Show("You're ahead!  Good job!");
            else MessageBox.Show("Sorry, You lost! Better hit the ATM!");
        }

        //Exit the form and display the exit message.
        private void button2_Click(object sender, EventArgs e)
        {
            exitMessage();
            this.Close();
        }
    }
}
