﻿namespace Project1
{
    partial class singleBike
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label vIN__Label;
            System.Windows.Forms.Label modelLabel;
            System.Windows.Forms.Label colorLabel;
            System.Windows.Forms.Label mileageLabel;
            System.Windows.Forms.Label priceLabel;
            System.Windows.Forms.Label yearLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(singleBike));
            this.motorcycleDataSet = new Project1.MotorcycleDataSet();
            this.motorcyclesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.motorcyclesTableAdapter = new Project1.MotorcycleDataSetTableAdapters.MotorcyclesTableAdapter();
            this.tableAdapterManager = new Project1.MotorcycleDataSetTableAdapters.TableAdapterManager();
            this.motorcyclesBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.motorcyclesBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.vIN__TextBox = new System.Windows.Forms.TextBox();
            this.modelTextBox = new System.Windows.Forms.TextBox();
            this.colorTextBox = new System.Windows.Forms.TextBox();
            this.mileageTextBox = new System.Windows.Forms.TextBox();
            this.priceTextBox = new System.Windows.Forms.TextBox();
            this.yearTextBox = new System.Windows.Forms.TextBox();
            this.accessoriesButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            vIN__Label = new System.Windows.Forms.Label();
            modelLabel = new System.Windows.Forms.Label();
            colorLabel = new System.Windows.Forms.Label();
            mileageLabel = new System.Windows.Forms.Label();
            priceLabel = new System.Windows.Forms.Label();
            yearLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.motorcycleDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motorcyclesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.motorcyclesBindingNavigator)).BeginInit();
            this.motorcyclesBindingNavigator.SuspendLayout();
            this.SuspendLayout();
            // 
            // vIN__Label
            // 
            vIN__Label.AutoSize = true;
            vIN__Label.Location = new System.Drawing.Point(67, 85);
            vIN__Label.Name = "vIN__Label";
            vIN__Label.Size = new System.Drawing.Size(38, 13);
            vIN__Label.TabIndex = 1;
            vIN__Label.Text = "VIN #:";
            // 
            // modelLabel
            // 
            modelLabel.AutoSize = true;
            modelLabel.Location = new System.Drawing.Point(67, 111);
            modelLabel.Name = "modelLabel";
            modelLabel.Size = new System.Drawing.Size(39, 13);
            modelLabel.TabIndex = 3;
            modelLabel.Text = "Model:";
            // 
            // colorLabel
            // 
            colorLabel.AutoSize = true;
            colorLabel.Location = new System.Drawing.Point(67, 137);
            colorLabel.Name = "colorLabel";
            colorLabel.Size = new System.Drawing.Size(34, 13);
            colorLabel.TabIndex = 5;
            colorLabel.Text = "Color:";
            // 
            // mileageLabel
            // 
            mileageLabel.AutoSize = true;
            mileageLabel.Location = new System.Drawing.Point(67, 163);
            mileageLabel.Name = "mileageLabel";
            mileageLabel.Size = new System.Drawing.Size(47, 13);
            mileageLabel.TabIndex = 7;
            mileageLabel.Text = "Mileage:";
            // 
            // priceLabel
            // 
            priceLabel.AutoSize = true;
            priceLabel.Location = new System.Drawing.Point(67, 189);
            priceLabel.Name = "priceLabel";
            priceLabel.Size = new System.Drawing.Size(34, 13);
            priceLabel.TabIndex = 9;
            priceLabel.Text = "Price:";
            // 
            // yearLabel
            // 
            yearLabel.AutoSize = true;
            yearLabel.Location = new System.Drawing.Point(67, 215);
            yearLabel.Name = "yearLabel";
            yearLabel.Size = new System.Drawing.Size(32, 13);
            yearLabel.TabIndex = 11;
            yearLabel.Text = "Year:";
            // 
            // motorcycleDataSet
            // 
            this.motorcycleDataSet.DataSetName = "MotorcycleDataSet";
            this.motorcycleDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // motorcyclesBindingSource
            // 
            this.motorcyclesBindingSource.DataMember = "Motorcycles";
            this.motorcyclesBindingSource.DataSource = this.motorcycleDataSet;
            // 
            // motorcyclesTableAdapter
            // 
            this.motorcyclesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.MotorcyclesTableAdapter = this.motorcyclesTableAdapter;
            this.tableAdapterManager.UpdateOrder = Project1.MotorcycleDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // motorcyclesBindingNavigator
            // 
            this.motorcyclesBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.motorcyclesBindingNavigator.BindingSource = this.motorcyclesBindingSource;
            this.motorcyclesBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.motorcyclesBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.motorcyclesBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.motorcyclesBindingNavigatorSaveItem});
            this.motorcyclesBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.motorcyclesBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.motorcyclesBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.motorcyclesBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.motorcyclesBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.motorcyclesBindingNavigator.Name = "motorcyclesBindingNavigator";
            this.motorcyclesBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.motorcyclesBindingNavigator.Size = new System.Drawing.Size(349, 25);
            this.motorcyclesBindingNavigator.TabIndex = 0;
            this.motorcyclesBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // motorcyclesBindingNavigatorSaveItem
            // 
            this.motorcyclesBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.motorcyclesBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("motorcyclesBindingNavigatorSaveItem.Image")));
            this.motorcyclesBindingNavigatorSaveItem.Name = "motorcyclesBindingNavigatorSaveItem";
            this.motorcyclesBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.motorcyclesBindingNavigatorSaveItem.Text = "Save Data";
            // 
            // vIN__TextBox
            // 
            this.vIN__TextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.motorcyclesBindingSource, "VIN #", true));
            this.vIN__TextBox.Location = new System.Drawing.Point(140, 82);
            this.vIN__TextBox.Name = "vIN__TextBox";
            this.vIN__TextBox.ReadOnly = true;
            this.vIN__TextBox.Size = new System.Drawing.Size(100, 20);
            this.vIN__TextBox.TabIndex = 2;
            // 
            // modelTextBox
            // 
            this.modelTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.motorcyclesBindingSource, "Model", true));
            this.modelTextBox.Location = new System.Drawing.Point(140, 108);
            this.modelTextBox.Name = "modelTextBox";
            this.modelTextBox.ReadOnly = true;
            this.modelTextBox.Size = new System.Drawing.Size(100, 20);
            this.modelTextBox.TabIndex = 4;
            // 
            // colorTextBox
            // 
            this.colorTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.motorcyclesBindingSource, "Color", true));
            this.colorTextBox.Location = new System.Drawing.Point(140, 134);
            this.colorTextBox.Name = "colorTextBox";
            this.colorTextBox.ReadOnly = true;
            this.colorTextBox.Size = new System.Drawing.Size(100, 20);
            this.colorTextBox.TabIndex = 6;
            // 
            // mileageTextBox
            // 
            this.mileageTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.motorcyclesBindingSource, "Mileage", true));
            this.mileageTextBox.Location = new System.Drawing.Point(140, 160);
            this.mileageTextBox.Name = "mileageTextBox";
            this.mileageTextBox.ReadOnly = true;
            this.mileageTextBox.Size = new System.Drawing.Size(100, 20);
            this.mileageTextBox.TabIndex = 8;
            // 
            // priceTextBox
            // 
            this.priceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.motorcyclesBindingSource, "Price", true));
            this.priceTextBox.Location = new System.Drawing.Point(140, 186);
            this.priceTextBox.Name = "priceTextBox";
            this.priceTextBox.ReadOnly = true;
            this.priceTextBox.Size = new System.Drawing.Size(100, 20);
            this.priceTextBox.TabIndex = 10;
            // 
            // yearTextBox
            // 
            this.yearTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.motorcyclesBindingSource, "Year", true));
            this.yearTextBox.Location = new System.Drawing.Point(140, 212);
            this.yearTextBox.Name = "yearTextBox";
            this.yearTextBox.ReadOnly = true;
            this.yearTextBox.Size = new System.Drawing.Size(100, 20);
            this.yearTextBox.TabIndex = 12;
            // 
            // accessoriesButton
            // 
            this.accessoriesButton.Location = new System.Drawing.Point(140, 257);
            this.accessoriesButton.Name = "accessoriesButton";
            this.accessoriesButton.Size = new System.Drawing.Size(75, 42);
            this.accessoriesButton.TabIndex = 13;
            this.accessoriesButton.Text = "Show Accessories";
            this.accessoriesButton.UseVisualStyleBackColor = true;
            this.accessoriesButton.Click += new System.EventHandler(this.accessoriesButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Location = new System.Drawing.Point(140, 318);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 14;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // singleBike
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 438);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.accessoriesButton);
            this.Controls.Add(vIN__Label);
            this.Controls.Add(this.vIN__TextBox);
            this.Controls.Add(modelLabel);
            this.Controls.Add(this.modelTextBox);
            this.Controls.Add(colorLabel);
            this.Controls.Add(this.colorTextBox);
            this.Controls.Add(mileageLabel);
            this.Controls.Add(this.mileageTextBox);
            this.Controls.Add(priceLabel);
            this.Controls.Add(this.priceTextBox);
            this.Controls.Add(yearLabel);
            this.Controls.Add(this.yearTextBox);
            this.Controls.Add(this.motorcyclesBindingNavigator);
            this.Name = "singleBike";
            this.Text = "singleBike";
            this.Load += new System.EventHandler(this.singleBike_Load);
            ((System.ComponentModel.ISupportInitialize)(this.motorcycleDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motorcyclesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.motorcyclesBindingNavigator)).EndInit();
            this.motorcyclesBindingNavigator.ResumeLayout(false);
            this.motorcyclesBindingNavigator.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MotorcycleDataSet motorcycleDataSet;
        private System.Windows.Forms.BindingSource motorcyclesBindingSource;
        private MotorcycleDataSetTableAdapters.MotorcyclesTableAdapter motorcyclesTableAdapter;
        private System.Windows.Forms.BindingNavigator motorcyclesBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton motorcyclesBindingNavigatorSaveItem;
        private System.Windows.Forms.TextBox vIN__TextBox;
        private System.Windows.Forms.TextBox modelTextBox;
        private System.Windows.Forms.TextBox colorTextBox;
        private System.Windows.Forms.TextBox mileageTextBox;
        private System.Windows.Forms.TextBox priceTextBox;
        private System.Windows.Forms.TextBox yearTextBox;
        private System.Windows.Forms.Button accessoriesButton;
        private System.Windows.Forms.Button closeButton;
        private MotorcycleDataSetTableAdapters.TableAdapterManager tableAdapterManager;
    }
}