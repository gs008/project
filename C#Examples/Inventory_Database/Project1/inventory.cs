﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public partial class inventory : Form
    {
        public inventory()
        {
            InitializeComponent();
        }

        private void motorcyclesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.motorcyclesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.motorcycleDataSet);

        }

        private void inventory_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'motorcycleDataSet.Motorcycles' table. You can move, or remove it, as needed.
            this.motorcyclesTableAdapter.Fill(this.motorcycleDataSet.Motorcycles);

        }

        private void searchButton_Click(object sender, EventArgs e) //search inventory
        {
            this.motorcyclesTableAdapter.SearchAny(this.motorcycleDataSet.Motorcycles, textBox1.Text);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            //Close the Form
            this.Close();
        }

        private void yearButton_Click(object sender, EventArgs e)  //select by year
        {
            this.motorcyclesTableAdapter.YearSearch(this.motorcycleDataSet.Motorcycles);
        }

        private void priceButton_Click(object sender, EventArgs e) //select by price
        {
            this.motorcyclesTableAdapter.PriceSearch(this.motorcycleDataSet.Motorcycles);
        }

        private void modelButton_Click(object sender, EventArgs e)  //select by model
        {
            this.motorcyclesTableAdapter.ModelSearch(this.motorcycleDataSet.Motorcycles);
        }

        private void oldestButton_Click(object sender, EventArgs e)  //oldest bike
        {
            singleBike oldest = new singleBike();
            oldest.oldest();
            oldest.ShowDialog();
        }

        private void newestButton_Click(object sender, EventArgs e)  //newest bike
        {
            singleBike newest = new singleBike();  
            newest.newest();
            newest.ShowDialog();
        }

        private void selectButton_Click(object sender, EventArgs e) //extract values from selected row
        {
           if(motorcyclesDataGridView.SelectedRows.Count > 0)
            {
                string[] values = new string[7];  //holds column values
                values[0] = motorcyclesDataGridView.SelectedRows[0].Cells[0].Value + string.Empty;
                values[1] = motorcyclesDataGridView.SelectedRows[0].Cells[1].Value + string.Empty;
                values[2] = motorcyclesDataGridView.SelectedRows[0].Cells[2].Value + string.Empty;
                values[3] = motorcyclesDataGridView.SelectedRows[0].Cells[3].Value + string.Empty;
                values[4] = motorcyclesDataGridView.SelectedRows[0].Cells[4].Value + string.Empty;
                values[5] = motorcyclesDataGridView.SelectedRows[0].Cells[5].Value + string.Empty;
                values[6] = motorcyclesDataGridView.SelectedRows[0].Cells[6].Value + string.Empty;
                singleBike bike = new singleBike();
                bike.updateForm(values); //pass values to bike form
                bike.ShowDialog();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)  //search textbox
        {
            this.motorcyclesTableAdapter.SearchAny(this.motorcycleDataSet.Motorcycles, textBox1.Text);
        }
    }
}
