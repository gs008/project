﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public partial class singleBike : Form
    {
        public string Accessories { get; set; }
        public singleBike()
        {
            InitializeComponent();
        }

        private void motorcyclesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.motorcyclesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.motorcycleDataSet);

        }

        private void singleBike_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'motorcycleDataSet.Motorcycles' table. You can move, or remove it, as needed.
            //this.motorcyclesTableAdapter.Fill(this.motorcycleDataSet.Motorcycles);
        }

        public void oldest()
        {
            int oldest = (int)this.motorcyclesTableAdapter.oldestYear();
            this.motorcyclesTableAdapter.updateOldestYear(this.motorcycleDataSet.Motorcycles, oldest);
            Accessories = this.motorcyclesTableAdapter.updateAcc(oldest).ToString();

        }
        public void newest()
        {
            int newest = (int)this.motorcyclesTableAdapter.newestYear();
            this.motorcyclesTableAdapter.updateOldestYear(this.motorcycleDataSet.Motorcycles, newest);
            Accessories = this.motorcyclesTableAdapter.updateAcc(newest).ToString();
        }

        public void updateForm(string[] myArray)
        { 
                vIN__TextBox.Text = myArray[0];
                modelTextBox.Text = myArray[1];
                colorTextBox.Text = myArray[2];
                mileageTextBox.Text = myArray[3];
                priceTextBox.Text = myArray[4];
                yearTextBox.Text = myArray[5];
                Accessories = acc(myArray[6]);           
        }
        private string acc(string acc)
        {
            return acc;
        }

        private void accessoriesButton_Click(object sender, EventArgs e)
        {            
            Accessories bikeAcc = new Accessories();
            bikeAcc.updateListBox(Accessories);
            bikeAcc.ShowDialog();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
