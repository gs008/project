﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project1
{
    public partial class Accessories : Form
    {
        public Accessories()
        {
            InitializeComponent();
        }

        public void updateListBox(string accessories)
        {
            char[] delim = { ',' };
            string[] tokens = accessories.Split(delim);
            foreach (string str in tokens)
            {
                string s = str;
                s = str.Trim();
                listBox1.Items.Add(s);
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            //Close the form
            this.Close();
        }
    }
}
