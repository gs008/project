# README #

View some of the work that I've done while being a student in the [Web Systems Program](http://icc.edu/academics/catalog/business-hospitality-and-information-systems/?prog=Web%20Systems) at ICC.

### Personal Project Portfolio ###

* While attending Illinois Central College, I have learned a wide variety of skills which I hope to expand on as I progress towards finding a solid learning environment and job. 
Here you can see a few of the back end projects that I've worked on while in the program.


###Choose a project ###

* If you see something that sparks your interest, download it and run in Visual Studio (C#)
* Or download an APK file to your Android smartphone or tablet to try the Phonegap apps


### Contact ###

* Greg Sendelbach <mailto:sndlbk82@aol.com>